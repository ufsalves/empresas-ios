//
//  LoginView.swift
//  Empresas
//
//  Created by Ultimo Alves on 14/01/20.
//  Copyright © 2020 Ultimo Alves. All rights reserved.
//

import UIKit
import Stevia

protocol LoginViewDelegate: NSObjectProtocol {
    func loginAction()
}

class LoginView: UIView {
    weak var delegate: LoginViewDelegate?
    
    private lazy var logoImg: UIImageView = {
        let logoImg = UIImageView(frame: .zero)
        logoImg.image = UIImage(named: "logo_ioasys")
        logoImg.sizeToFit()
        return logoImg
    }()
    
    private lazy var emailLabel: UILabel = {
        let emailLabel = UILabel(frame: .zero)
        emailLabel.textAlignment = .left
        emailLabel.textColor = .lightGray
        emailLabel.text = "Email"
        emailLabel.font = UIFont(name: "Arial", size: 18)
        return emailLabel
    }()
    
    private lazy var passwordLabel: UILabel = {
        let passwordLabel = UILabel(frame: .zero)
        passwordLabel.textAlignment = .left
        passwordLabel.textColor = .lightGray
        passwordLabel.text = "Password"
        passwordLabel.font = UIFont(name: "Arial", size: 18)
        return passwordLabel
    }()
    
    private lazy var emailTextField: UITextField = {
        let emailTextField = UITextField(frame: .zero)
        emailTextField.borderStyle = .roundedRect
        emailTextField.autocapitalizationType = .none
        emailTextField.font = UIFont(name: "Arial", size: 20)
        emailTextField.textContentType = .emailAddress
        emailTextField.keyboardType = .emailAddress
        return emailTextField
    }()
    
    private lazy var passwordTextField: UITextField = {
        let passwordTextField = UITextField(frame: .zero)
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.textContentType = .password
        passwordTextField.font = UIFont(name: "Arial", size: 20)
        return passwordTextField
    }()
    
    private lazy var loginButton: UIButton = {
        let loginButton = UIButton(frame: .zero)
        loginButton.backgroundColor = .blue
        loginButton.layer.cornerRadius = 15
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.yellow.cgColor
        loginButton.setTitle("Login", for: .normal)
        loginButton.titleLabel?.font = UIFont(name: "Arial-Black", size: 20)
        return loginButton
    }()
    
    private lazy var messageLabel: UILabel = {
        let messageLabel = UILabel(frame: .zero)
        messageLabel.textAlignment = .center
        messageLabel.textColor = .white
        messageLabel.text = "Password or Email incorrect. Please try again"
        messageLabel.font = UIFont(name: "Arial", size: 18)
        messageLabel.backgroundColor = .red
        return messageLabel
    }()
    
    private let activityView = UIActivityIndicatorView(style: .large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
      }
    
      required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
      }
    
    private func commonInit() {
        subviews()
        layout()
        addActions()
    }
    
    private func subviews() {
        sv([logoImg, emailLabel, emailTextField, passwordLabel, passwordTextField, loginButton, messageLabel])
    }
    
    private func layout() {
        logoImg.centerHorizontally().width(180).height(100).top(20%)
        
        emailLabel.Top == logoImg.Bottom+50
        emailLabel.left(17.5%)
        
        emailTextField.Top == emailLabel.Bottom+5
        emailTextField.Left == emailLabel.Left
        emailTextField.width(65%)
        
        passwordLabel.Top == emailTextField.Bottom+25
        passwordLabel.Left == emailLabel.Left
        
        passwordTextField.Top == passwordLabel.Bottom+5
        passwordTextField.Left == emailLabel.Left
        passwordTextField.width(65%)
        
        loginButton.centerHorizontally().width(200).height(50).Top == passwordTextField.Bottom+60
        
        messageLabel.Bottom == loginButton.Top-20
        messageLabel.centerHorizontally()
        
        messageLabel.isHidden = true
    }
    
    private func addActions() {
        loginButton.addTarget(self, action: #selector(loginAction), for: .touchDown)
    }
    
    @objc func loginAction(){
        addSubview(activityView)
        loginButton.setTitle("", for: .normal)
        activityView.center = loginButton.center
        loginButton.isEnabled = false
        activityView.startAnimating()
        delegate?.loginAction()
    }
    
    func setTextFieldDelegate(delegate : LoginViewController){
        emailTextField.delegate = delegate
        passwordTextField.delegate = delegate
    }
    
    func loginEnded(result : Bool){
        DispatchQueue.main.async {
             self.messageLabel.alpha = 0.0
        
            if(!result){
                self.messageLabel.text = "Password or Email incorrect. Please try again"
                self.messageLabel.backgroundColor = .red
            }else {
                self.messageLabel.text = "Login Successful"
                self.messageLabel.backgroundColor = .green
            }
            
            UIView.animate(withDuration: 1.5, animations: {
                self.messageLabel.isHidden = false
                self.messageLabel.alpha = 1.0
            })
            
            self.activityView.removeFromSuperview()
            self.loginButton.setTitle("Login", for: .normal)
            self.loginButton.isEnabled = true}
    }
    
    func hideMessageLabel(){
        self.messageLabel.isHidden = true
    }
    
    func getEmailText() -> String{
        return emailTextField.text ?? ""
    }
    
    func getPasswordText() -> String{
        return passwordTextField.text ?? ""
    }
    
}
