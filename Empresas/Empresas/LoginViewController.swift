//
//  LoginViewController.swift
//  Empresas
//
//  Created by Ultimo Alves on 14/01/20.
//  Copyright © 2020 Ultimo Alves. All rights reserved.
//

import UIKit
import Stevia
class LoginViewController: UIViewController {

    private let loginView : LoginView = LoginView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.sv(loginView)
        loginView.delegate = self
        loginView.fillContainer()
        loginView.setTextFieldDelegate(delegate: self)
    }
    
    func serviceLogin(urlToRequest: String, email: String, password: String){
        //   testeapple@ioasys.com.br  12341234
        var request = URLRequest(url:URL(string:urlToRequest)!)
        let postString = ["email":email, "password": password]
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
               guard(err == nil) else {
                   print("\(String(describing: err))")
                   return
               }
              
               guard let data = data else{
                   print("no data return")
                   return
               }
               
              
                let parseResult: [String:AnyObject]!
                do{
                    parseResult = try (JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject])
                    if let success = parseResult["success"]{
                        let result = success as! Bool
                        self.loginView.loginEnded(result: result)
                        if result {
                            print(parseResult)
                        }
                    }
                    
                    
                } catch {
                       print("Could not parse data as Json \(data)")
                       return
                }
               
            }.resume()
       }
}

extension LoginViewController : LoginViewDelegate{
    func loginAction() {
        serviceLogin(urlToRequest:"https://empresas.ioasys.com.br/api/v1/users/auth/sign_in", email: loginView.getEmailText(), password: loginView.getPasswordText())
    }
}
extension LoginViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        loginView.hideMessageLabel()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
